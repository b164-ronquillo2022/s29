        //mock database
		//console.log('hello world')

		const express = require("express");
		const app = express(); // Server
		const PORT = 2022;

		app.use(express.json());
		app.use(express.urlencoded({extended:true}));

		//sol 1
		app.get("/home", (req, res) => res.status(200).send(`Welcome to Homepage!`))

		//sol 3
		let users = [
		    {
		    	"userName": "Catriona Gray",
		    	 "password": "admin121"
		    },
		    {
		    	"userName": "GiGi de lana",
		    	 "password": "admin122"
		    },
		    {
		    	"userName": "Liza Soberano",
		         "password": "admin123"
		     }
		] 
		app.get("/users", (req, res) => {
		    res.status(200).send(users);
		})

		//sol 5
		app.delete("/delete-user", (req, res) => {
		    const {userName, password} = req.body;
		    let message="";

		    for(let i = 0; i < users.length; i++){
				if(users[i].userName === userName){
					let delUser = users.splice([i],1);
					message = `User ${delUser[0].userName} has been deleted.`;
		            console.log(delUser);
					break;
				} else {
					message = `User does not exist.`;
				}
			}
		    console.log(users);
		    res.status(200).send(message);
		})

		app.listen(PORT, () => console.log(`Server connected to port ${PORT}`));